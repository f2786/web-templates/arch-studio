import { css } from "styled-components";

export const StyledSection = css`
    max-width: var(--system-width-desktop);
    height: 100%;
    margin: 0 auto;
    position: relative;

    @media screen and (max-width: 1280px) {
        max-width: var(--system-width-desktop-small);
    }

    @media screen and (max-width: 1180px) {
        max-width: var(--system-width-tablet);
    }
`;
