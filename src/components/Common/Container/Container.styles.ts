import styled from "styled-components";

export const StyledContainer = styled.div`
    position: relative;
    margin: 0 auto;
    width: 100vw;
    padding: 8rem 1.5rem;

    @media screen and (min-width: 400px) {
        padding: 8rem 1.5rem;
    }
    @media screen and (min-width: 720px) {
        max-width: var(--system-width-5);
        padding: 8rem 0;
    }
    @media screen and (min-width: 1024px) {
        max-width: var(--system-width-6);
    }
    @media screen and (min-width: 1280px) {
        max-width: var(--system-width-7);
    }
    @media screen and (min-width: 1440px) {
        max-width: var(--system-width-7);
    }
`;
