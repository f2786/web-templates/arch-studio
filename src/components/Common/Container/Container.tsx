import React from "react";
import { StyledContainer } from "./Container.styles";

// eslint-disable-next-line max-len
const Container = React.forwardRef<HTMLDivElement, React.ComponentProps<typeof StyledContainer>>(({ children, ...props }, ref) => (
  <StyledContainer ref={ref} {...props}>
    {children}
  </StyledContainer>
));

export default Container;
