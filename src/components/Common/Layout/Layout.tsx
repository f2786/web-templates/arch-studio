import React from "react";
import { Footer } from "../Footer";
import { Navbar } from "../Navbar";
import { Main } from "./Layout.styles";

const Layout: React.FC = ({ children }) => (
  <>
    <Navbar />
    <Main>
      {children}
    </Main>
    <Footer />
  </>
);

export default Layout;
