import styled from "styled-components";

export const Main = styled.main`
    margin-top: 9.5rem;

    @media screen and (max-width: 768px) {
        margin-top: 6rem;
    }
`;
