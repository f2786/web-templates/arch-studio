import React from "react";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import {
  ContainerStyled,
  NavLinkStyled,
  LeftContainerStyled,
  ImgStyled,
  RightContainerStyled,
  RightContainerItemStyled,
  NavBarWrapper,
} from "./Navbar.styles";
import { useToggleTheme } from "../../../hooks";

const Navbar = () => {
  const { t, lang } = useTranslation("common");
  // eslint-disable-next-line no-unused-vars
  const [_, toggleTheme] = useToggleTheme();
  const lng = lang === "en" ? "es" : "en";

  return (
    <NavBarWrapper>
      {/* <BackgroundStyled /> */}
      <ContainerStyled>
        <ImgStyled src="/logo.svg" alt="Logo" />
        <LeftContainerStyled>
          <NavLinkStyled href="/">{t`footer.portfolio`}</NavLinkStyled>
          <NavLinkStyled href="/">{t`footer.about-us`}</NavLinkStyled>
          <NavLinkStyled href="/">{t`footer.contact`}</NavLinkStyled>
        </LeftContainerStyled>
        <RightContainerStyled>
          <RightContainerItemStyled>
            <Link href="/" locale={lng}>
              locale
            </Link>
          </RightContainerItemStyled>
          <RightContainerItemStyled
            onClick={() => toggleTheme?.()}
          >
            theme
          </RightContainerItemStyled>
        </RightContainerStyled>
      </ContainerStyled>
    </NavBarWrapper>
  );
};

export default Navbar;
