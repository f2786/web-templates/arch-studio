import styled from "styled-components";
import { Link } from "../../Ui/Link";

export const NavBarWrapper = styled.nav`
    margin: 0 auto;
    padding: 3.5rem 0;
    width: 100vw;
    display: flex;
    /* justify-content: center; */
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1;
    background-color: var(--system-color-page);

    @media screen and (max-width: 768px) {
        padding: 2rem 0;
    }
`;

export const ContainerStyled = styled.div`
    width: var(--system-width-desktop);
    display: flex;
    justify-content: start;
    align-items: center;
    margin: 0 auto;

    @media screen and (max-width: 1280px) {
        width: var(--system-width-desktop-small);
    }
    @media screen and (max-width: 1180px) {
        width: var(--system-width-tablet);
    }
    @media screen and (max-width: 960px) {
        width: var(--system-width-tablet);
    }
    @media screen and (max-width: 768px) {
        width: 100%;
        padding: 0 2rem;
    }
`;

export const ImgStyled = styled.img`
    filter: var(--system-color-logo-nav);
`;

export const LeftContainerStyled = styled.ul`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    align-items: center;

    gap: var(--system-spacing-4);
    padding: 0 var(--system-space-9);
    width: 100%;

    @media screen and (max-width: 768px) {
        display: none;
    }
`;

export const NavLinkStyled = styled(Link)`
    /* padding-right: var(--system-spacing-7); */
    color: var(--system-color-gray-medium);
    font-weight: var(--system-weight-bold);
    text-align: center;
`;

export const BackgroundStyled = styled.div`
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
`;

export const RightContainerStyled = styled.ul`
    display: flex;
    justify-content: flex-end;
    width: 50%;


    /* TODO */
    @media screen and (max-width: 1180px) {
        display: none;
    }
`;

export const RightContainerItemStyled = styled.li`
    margin-left: 2rem;
`;
