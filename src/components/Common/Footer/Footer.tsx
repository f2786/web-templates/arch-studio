import React from "react";
import useTranslation from "next-translate/useTranslation";
import {
  ButtonContainer,
  ButtonLinkStyled,
  FooterContainer, SectionWrapper, StyledImg, StyledItems, StyledLinkItem, StyledLogo,
} from "./Footer.styles";

const Footer = () => {
  const { t } = useTranslation("common");

  return (
    <SectionWrapper>
      <FooterContainer>
        <StyledLogo>
          <StyledImg src="/logo.svg" alt="Logo" />
        </StyledLogo>
        <StyledItems>
          <StyledLinkItem href="/">{t`nav.portfolio`}</StyledLinkItem>
          <StyledLinkItem href="/">{t`nav.about-us`}</StyledLinkItem>
          <StyledLinkItem href="/">{t`nav.contact`}</StyledLinkItem>
        </StyledItems>
        <ButtonContainer>
          <ButtonLinkStyled>
            {t`footer.see-portfolio`}
          </ButtonLinkStyled>
        </ButtonContainer>
      </FooterContainer>
    </SectionWrapper>
  );
};

export default Footer;
