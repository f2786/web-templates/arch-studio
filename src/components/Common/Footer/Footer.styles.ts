import styled from "styled-components";
import { ButtonLink, Link } from "../../Ui/Link";
import { StyledSection } from "../Section";

export const SectionWrapper = styled.footer`
    ${StyledSection}

    margin-top: 12rem;
`;

export const FooterContainer = styled.div`
    position: relative;
    display: flex;

    @media screen and (max-width: 768px ){
        display: grid;
        grid-template-columns: 1fr;
        grid-template-rows: repeat(2, max-content);
        justify-items: center;
        background-color: var(--system-color-footer);
    }
`;

export const StyledItems = styled.ul`
    display: grid;
    grid-template-columns: repeat(3, 1fr) 10rem;
    align-items: center;
    gap: var(--system-spacing-4);
    width: 70%;
    margin-right: var(--system-spacing-7);
    padding-left: var(--system-spacing-4);
    background-color: var(--system-color-footer);

    @media screen and (max-width: 768px ){
        display: grid;
        grid-template-columns: 1fr;
        grid-template-rows: repeat(3, 1fr);
        width: 100%;
        padding-top: 5rem;
        padding-left: 0;
        margin: 0;
        justify-items: center;
    }
`;

export const StyledLogo = styled.figure`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 12rem;
    width: 12rem;
    /* TODO To change */
    background-color: var(--system-color-title);

    @media screen and (max-width: 1180px) {
        height: 7rem;
        width: 7rem;
    }
    @media screen and (max-width: 768px ){
        position: absolute;
        top: -15%;
    }
`;

export const ButtonContainer = styled.div`
    position: absolute;
    right: 0;
    top: 30%;

    @media screen and (max-width: 1180px) {
        top: 17.5%;
    }

    @media screen and (max-width: 768px ){
        position: static;
        margin: 0 auto;
        margin-top: 3rem;
        margin-bottom: 3rem;
    }
`;

export const StyledImg = styled.img`
    filter: var(--system-color-logo-footer);

    @media screen and (max-width: 1180px) {
        /* height: 1rem; */
        width: 3em;
    }
`;

export const StyledLinkItem = styled(Link)`
    font-weight: var(--system-weight-bold);
    text-align: center;

    @media screen and (max-width: 1180px) {
        width: max-content;
    }
`;

export const ButtonLinkStyled = styled(ButtonLink)`
    background-color: var(--system-color-button-page);

    :hover {
        background-color: var(--system-color-gray-dark);
    }

    img {
        filter: var(--system-color-button-page-logo);
    }

    span {
        color: var(--system-color-button-page-content);
    }
`;
