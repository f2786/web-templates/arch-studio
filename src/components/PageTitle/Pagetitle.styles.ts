import styled from "styled-components";
import { BodyText } from "../Ui/Text";

export const Wrapper = styled.div`
    position: relative;
    max-width: var(--system-width-desktop);
    margin: 0 auto;
    top: -9.5rem;
    background: #000;
    z-index: 10;

    @media screen and (max-width: 1280px) {
        max-width: var(--system-width-desktop-small);
    }
    @media screen and (max-width: 1180px) {
        max-width: var(--system-width-tablet);
    }
    @media screen and (max-width: 960px) {
        max-width: var(--system-width-tablet);
    }
    @media screen and (max-width: 768px) {
        display: none;
    }
`;

export const PageTitleStyled = styled.span`
    position: absolute;
    transform: rotate(90deg);
    top: 10rem;
    margin-left: -5rem;

    :before {
        position: absolute;
        top: 0.5em;
        left: -10rem;
        width: 6.5rem;
        height: 1px;
        background: var(--system-color-gray-light);
        content: "";
    }
`;

export const PageContentStyled = styled.p`
    ${BodyText}
    position: absolute;
    display: flex;
    color: var(--system-color-gray-light);
`;

export const PageItemsStyled = styled.span`
    padding-right: 1rem;
`;
