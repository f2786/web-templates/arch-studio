import PropTypes from "prop-types";
import React from "react";
import {
  PageContentStyled, PageItemsStyled, PageTitleStyled, Wrapper,
} from "./Pagetitle.styles";

interface Props {
  title: string
}

const PageTitle: React.FC<Props> = ({ title }) => (
  <Wrapper>
    <PageTitleStyled>
      <PageContentStyled>
        {title.toUpperCase().split("").map((char) => (
          <PageItemsStyled key={char}>{char}</PageItemsStyled>
        ))}
      </PageContentStyled>
    </PageTitleStyled>
  </Wrapper>
);

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PageTitle;
