import React from "react";
import useTranslation from "next-translate/useTranslation";
import { Card } from "./Card";
import {
  ButtonLinkStyled,
  CardsContainer, HeaderWrapper, SectionWrapper, StyledTitle,
} from "./FeaturedProjects.styles";

interface PortfolioData {
  title: string;
  description: string;
  src: string;
  srcSet: string;
  alt: string;
}

interface FeaturedProjectsProps {
  content: PortfolioData[]
}

const FeaturedProjects: React.FC<FeaturedProjectsProps> = ({ content }) => {
  const { t } = useTranslation("common");

  return (
    <SectionWrapper>
      <HeaderWrapper>
        <StyledTitle>
          {t`featured.title`}
        </StyledTitle>
        <ButtonLinkStyled>
          {t`featured.button-content`}
        </ButtonLinkStyled>
      </HeaderWrapper>
      <CardsContainer>
        {content.map((project, i) => <Card key={project.title} number={i + 1} {...project} />)}
      </CardsContainer>
    </SectionWrapper>
  );
};

export default FeaturedProjects;
