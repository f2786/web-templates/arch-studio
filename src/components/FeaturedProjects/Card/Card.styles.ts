import styled from "styled-components";
import { HeadTitle, BodyText } from "../../Ui/Text/Text";
import { Caption } from "../../Ui/Text";

export const CardWrapper = styled.figure`
    height: 100%;
    overflow: hidden;
    position: relative;
`;

export const TextContainer = styled.div`
    height: 50%;
    width: 100%;
    position: absolute;
    bottom: 0;
    left: 0;
    background: linear-gradient(180deg, rgba(0, 0, 0, 0.000001) 0%, rgba(0, 0, 0, 0.7) 100%);
`;

export const TextWrapper = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    padding: 3rem;
`;

export const StyledTitle = styled.h3`
    ${Caption}

    color: white;
`;

export const StyledDescription = styled.p`
    ${BodyText}

    color: white;
`;

export const StyledNumber = styled.h1`
    ${HeadTitle}

    position: absolute;
    top: 0;
    right: 0;
    padding-top: 1rem;

    color: #ffffff;

    mix-blend-mode: normal;
    opacity: 0.5;

    @media screen and (max-width: 640px) {
        display: none;
    }
`;

export const StyledImg = styled.img`
    height: 115%;
    width: auto;

    background-size: cover;
    z-index: -1;
    position: absolute;
    top: 0;

    @media screen and (max-width: 960px) {
        width: 100%;
        height: auto;
    }

    @media screen and (max-width: 640px) {
        width: auto;
        height: 100%;
    }
`;
