import PropTypes from "prop-types";
import React from "react";
import {
  CardWrapper, StyledDescription, StyledImg, StyledNumber, StyledTitle, TextContainer, TextWrapper,
} from "./Card.styles";

interface CardProps {
  number: number;
  title: string;
  description: string;
  src: string;
  srcSet: string;
  alt: string;
}

const Card: React.FC<CardProps> = ({
  number,
  title,
  description,
  src,
  srcSet,
  alt,
}) => (
  <CardWrapper>
    <StyledImg src={src} srcSet={srcSet} alt={alt} />
    <TextContainer>
      <TextWrapper>
        <StyledTitle>{title}</StyledTitle>
        <StyledDescription>{description}</StyledDescription>
      </TextWrapper>
    </TextContainer>
    <StyledNumber>{number}</StyledNumber>
  </CardWrapper>
);

Card.propTypes = {
  number: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};

export default Card;
