import styled from "styled-components";
import { StyledSection } from "../Common/Section";
import { ButtonLink } from "../Ui/Link";
import { SubTitle } from "../Ui/Text";

export const SectionWrapper = styled.section`
    ${StyledSection}
    margin: 6rem auto;

    @media screen and (max-width: 960px) {
        max-width: var(--system-width-tablet);
    }

    @media screen and (max-width: 768px) {
        padding: 0 2rem;
    }
`;

export const HeaderWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const StyledTitle = styled.h1`
    ${SubTitle}

    color: var(--system-color-title);

`;

export const CardsContainer = styled.div`
    width: 100%;
    overflow: hidden;
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 1rem;
    margin-top: 4rem;
    height: 560px;

    @media screen and (max-width: 960px) {
        grid-template-columns: 1fr;
        grid-template-rows: repeat(3, 240px);
        gap: 2rem;
        height: 100%;
    }
`;

export const ButtonLinkStyled = styled(ButtonLink)`
    background-color: var(--system-color-button-page);

    :hover {
        background-color: var(--system-color-gray-dark);
    }

    img {
        filter: var(--system-color-button-page-logo);
    }

    span {
        color: var(--system-color-button-page-content);
    }

    @media screen and (max-width: 768px) {
        display: none;
    }
`;
