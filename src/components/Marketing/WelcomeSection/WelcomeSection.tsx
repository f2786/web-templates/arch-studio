import React from "react";
import useTranslation from "next-translate/useTranslation";
import {
  ContentWrapper, SectionWrapper, StyledHeadTitle, StyledImg, StyledTitle, Text, TextWrapper,
} from "./WelcomeSection.styles";

const WelcomeSection = () => {
  const { t } = useTranslation("home");

  return (
    <SectionWrapper>
      <StyledHeadTitle>Welcome</StyledHeadTitle>
      <ContentWrapper>
        <StyledTitle>{t`welcome-section.title`}</StyledTitle>
        <TextWrapper>
          <Text>
            {t`welcome-section.p1`}
          </Text>
          <Text>
            {t`welcome-section.p2`}
          </Text>
          <Text>
            {t`welcome-section.p3`}
          </Text>
        </TextWrapper>
      </ContentWrapper>
      <StyledImg src="/images/home/desktop/welcome.jpg" alt="building" />
    </SectionWrapper>
  );
};

export default WelcomeSection;
