import styled from "styled-components";
import { StyledSection } from "../../Common/Section";
import { BodyText, HeadTitle, Title } from "../../Ui/Text";

export const SectionWrapper = styled.section`
    ${StyledSection}
    margin: 6rem auto;

    @media screen and (max-width: 960px) {
        max-width: var(--system-width-tablet-small);
    }
    @media screen and (max-width: 768px) {
        padding: 0 2rem;
    }
`;

export const StyledHeadTitle = styled.h1`
    ${HeadTitle}

    color: var(--system-color-gray-2light);
    width: max-content;

    @media screen and (max-width: 1280px) {
        margin-left: -5rem;
    }

    @media screen  and (max-width: 1180px) {
        transform: scale(0.8);
    }

    @media screen and (max-width: 960px) {
        transform: scale(0.6);
        margin-left: -10.5rem;
    }

    @media screen and (max-width: 768px) {
        display: none;
    }
`;

export const StyledTitle = styled.h1`
    ${Title}

    color: var(--system-color-title);

    @media screen and (max-width: 1280px){
        text-align: center;
    }
    @media screen and (max-width: 690px){
        max-width: var(--system-width-mobile);
        font-size: 3rem;
    }
    @media screen and (max-width: 420px){
        font-size: 2.25rem;
        text-align: left;
    }
`;

export const ContentWrapper = styled.div`
    max-width: 510px;
    margin-left: 12rem;

    @media screen and (max-width: 1280px){
        margin: 0;
    }

    @media screen and (max-width: 1180px){
        margin: 0 auto;
    }
`;

export const TextWrapper = styled.div`
    margin-top: 3rem;
`;

export const Text = styled.p`
    ${BodyText}

    color: var(--system-color-gray-dark);
    margin-top: 1rem;
`;

export const StyledImg = styled.img`
    position: absolute;
    bottom: 0;
    right: 0;
    z-index: -1;

    @media screen and (max-width: 1180px ){
        display: none;
    }
`;
