import styled from "styled-components";
import { StyledSection } from "../../Common/Section";
import { ButtonLink } from "../../Ui/Link";
import { SubTitle } from "../../Ui/Text";

export const SectionWrapper = styled.section`
    ${StyledSection}

    height: 560px;
    margin: 12rem auto;

    @media screen and (max-width: 960px) {
        max-width: var(--system-width-tablet);
    }
    @media screen and (max-width: 768px) {

    }
`;

export const StyledTitle = styled.h1`
    ${SubTitle}

    color: white;
`;

export const ImgWrapper = styled.div`
    overflow: hidden;
    position: relative;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
`;

export const StyledImg = styled.img`
    position: absolute;
    /* background-size: cover; */
    z-index: -1;
    /* width: 100%; */
    height: 100%;
`;

export const ContentWrapper = styled.div`
    height: 100%;
    position: absolute;
    top: 0;
    display: grid;
    gap: 2rem;
    align-content: center;
    max-width: 370px;
    margin-left: 11rem;

    @media screen and (max-width: 1180px) {
        margin-left: 6rem;
    }

    @media screen and (max-width: 960px) {
        margin-left: 4rem;
    }
`;

export const ButtonLinkStyled = styled(ButtonLink)`
    background-color: var(--system-color-button-image);

    span {
        color: var(--system-color-button-image-content);
    }

    img {
        filter: var(--system-color-button-image-logo);
    }

    :hover {
        background-color: var(--system-color-gray-dark);
    }
`;
