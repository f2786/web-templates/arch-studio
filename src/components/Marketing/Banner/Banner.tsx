import React from "react";
import useTranslation from "next-translate/useTranslation";
import {
  ButtonLinkStyled,
  ContentWrapper, ImgWrapper, SectionWrapper, StyledImg, StyledTitle,
} from "./Banner.styles";

const Banner = () => {
  const { t } = useTranslation("home");

  return (
    <SectionWrapper>
      <ImgWrapper>
        <StyledImg src="/images/home/desktop/small-team.jpg" />
      </ImgWrapper>
      <ContentWrapper>
        <StyledTitle>
          {t`team-banner.title`}
        </StyledTitle>
        <ButtonLinkStyled>
          {t`team-banner.button-content`}
        </ButtonLinkStyled>
      </ContentWrapper>
    </SectionWrapper>
  );
};

export default Banner;
