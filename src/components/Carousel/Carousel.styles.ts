import styled from "styled-components";
import { StyledSection } from "../Common/Section";
import { ButtonLink } from "../Ui/Link";
import { BodyText, Title } from "../Ui/Text";

export const SectionWrapper = styled.section`
    ${StyledSection}

    @media screen and (max-width: 960px) {
        max-width: var(--system-width-tablet);
    }
`;

export const ImgStyled = styled.img`
    position: absolute;
    background-size: cover;
    z-index: -1;
`;

export const ContentWrapper = styled.div`
    height: 720px;
    width: 100%;
    display: grid;
    gap: 3rem;
    align-content: center;
    padding-left: 12rem;
    background-color: rgba(0, 0, 0, 0.3);

    @media screen and (max-width: 1180px) {
        padding-left: 8rem;
    }
    @media screen and (max-width: 960px) {
        padding-left: 4rem;
    }
    @media screen and (max-width: 768px) {
        padding-left: 2rem;
    }
`;

export const TextWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const TitleStyled = styled.h1`
    ${Title}
    max-width: 445px;
    color: white;

    @media screen and (max-width: 480px) {
        font-size: 2.5rem;
    }
`;

export const DescriptionStyled = styled.p`
    ${BodyText}
    max-width: 445px;
    color: white;
    margin: 1rem 0;
`;

export const CarouselButtonContainer = styled.div`
    position: absolute;
    bottom: 0;
    left: -5rem;
    display: flex;
    box-shadow: var(--system-shadow-2);

    @media screen and (max-width: 960px) {
        display: none;
    }
`;

export const ButtonCarouselStyled = styled.button<{ activeContent: number; index: number }>`
    width: 5rem;
    height: 5rem;
    overflow: clip;
    cursor: pointer;
    font-weight: bold;

    background-color: ${({ activeContent, index }) =>
        activeContent === index
            ? "var(--system-color-button-carousel-active)"
            : "var(--system-color-button-carousel)"};
    color: ${({ activeContent, index }) =>
        activeContent === index
            ? "var(--system-color-button-carousel-content-active)"
            : "var(--system-color-button-carousel-content)"};

    :hover {
        background-color: ${({ activeContent, index }) =>
            activeContent === index
                ? "var(--system-color-button-carousel-hover-active)"
                : "var(--system-color-button-carousel-hover)"};
    }
`;

export const ButtonLinkStyled = styled(ButtonLink)`
    background-color: var(--system-color-button-image);

    img {
        filter: var(--system-color-button-image-logo);
    }

    span {
        color: var(--system-color-button-image-content);
    }

    :hover {
        background-color: var(--system-color-gray-dark);
    }
`;

export const CarouselContainer = styled.div`
    width: 100%;
    overflow: hidden;
    position: relative;
    height: 720px;
    box-shadow: var(--system-shadow-2);
`;
