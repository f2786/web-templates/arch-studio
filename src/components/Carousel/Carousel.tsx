import React, { useState } from "react";
import styled from "styled-components";
import {
  DescriptionStyled,
  ImgStyled,
  SectionWrapper,
  TitleStyled,
  CarouselButtonContainer,
  TextWrapper,
  ContentWrapper,
  ButtonLinkStyled,
  ButtonCarouselStyled,
  CarouselContainer,
} from "./Carousel.styles";

interface CarouselContentData {
  img: string;
  title: string;
  description: string;
  link: {
    url: string;
    content: string;
  }
}

interface CarouselProps {
  content: CarouselContentData[]
}

const Wrapper = styled.div<{ index: number, activeContent: number }>`
  position: absolute;
  top: 0;
  left: ${({ index, activeContent }) => (index === activeContent ? 0 : 100)}%;
  opacity: ${({ index, activeContent }) => (index === activeContent ? 1 : 0)};
  width: 100%;
  height: 100%;
  transition: opacity 0.25s cubic-bezier(.175,.885,.32,1.275);
`;

function createContext<ContextValueType extends object | null>(
  rootComponentName: string,
  defaultContext?: ContextValueType,
) {
  const Context = React.createContext<ContextValueType | undefined>(defaultContext);

  const Provider = (props: ContextValueType & { children: React.ReactNode }) => {
    const { children, ...context } = props;
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const value = React.useMemo(() => context, Object.values(context)) as ContextValueType;
    return <Context.Provider value={value}>{children}</Context.Provider>;
  };

  function useContext(consumerName: string) {
    const context = React.useContext(Context);
    if (context) return context;
    if (defaultContext !== undefined) return defaultContext;
    throw new Error(`\`${consumerName}\` must be used within \`${rootComponentName}\``);
  }

  Provider.displayName = `${rootComponentName}Provider`;
  return [Provider, useContext] as const;
}

const [Provider, useCarouselContext] = createContext<{ content: CarouselContentData[], activeContent: number, setActiveContent: React.Dispatch<React.SetStateAction<number>> }>("Carousel");

const CarouselContent: React.FC = () => {
  const { content, activeContent } = useCarouselContext("CarouselContent");
  return (
    <CarouselContainer>
      {content.map(({
        img, title, description, link,
      }, index) => (
        <Wrapper
          key={title}
          index={index}
          activeContent={activeContent}
        >
          <ImgStyled src={img} />
          <ContentWrapper>
            <TextWrapper>
              <TitleStyled>
                {title}
              </TitleStyled>
              <DescriptionStyled>
                {description}
              </DescriptionStyled>
            </TextWrapper>
            <ButtonLinkStyled>
              {link.content}
            </ButtonLinkStyled>
          </ContentWrapper>
        </Wrapper>
      ))}
    </CarouselContainer>
  );
};

const CarouselSelector: React.FC = () => {
  const { content, activeContent, setActiveContent } = useCarouselContext("CarouselContent");

  const handleSelection = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, i: number): void => {
    e.preventDefault();
    if (i !== activeContent) {
      setActiveContent(i);
    }
  };

  return (
    <CarouselButtonContainer>
      {content.map(({ title }, i) => (
        <ButtonCarouselStyled
          key={title}
          onClick={(e) => handleSelection(e, i)}
          activeContent={activeContent}
          index={i}
        >
          {i + 1}
        </ButtonCarouselStyled>
      ))}
    </CarouselButtonContainer>
  );
};

const Carousel: React.FC<CarouselProps> = ({ content }) => {
  const [activeContent, setActiveContent] = useState<number>(0);

  return (
    <Provider
      content={content}
      activeContent={activeContent}
      setActiveContent={setActiveContent}
    >
      <SectionWrapper>
        <CarouselContent />
        <CarouselSelector />
      </SectionWrapper>
    </Provider>
  );
};

export default Carousel;
