import { css } from "styled-components";

export const HeadTitle = css`
  font-weight: var(--system-weight-bold);
  font-size: var(--system-text-11xl);
  letter-spacing: -0.005em;
  line-height: 1;
`;

export const Title = css`
  font-weight: var(--system-weight-bold);
  font-size: var(--system-text-5xl);
  letter-spacing: -0.002em;
  line-height: 1;
`;

export const SubTitle = css`
  font-weight: var(--system-weight-bold);
  font-size: var(--system-text-4xl); 
  letter-spacing: -0.002em;
  line-height: 1;
`;

export const Caption = css`
  font-weight: var(--system-weight-bold);
  font-size: var(--system-text-b); 
  line-height: var(--system-text-body-line-height);
`;

export const BodyText = css`
  font-weight: var(--system-weight-medium);
  font-size: var(--system-text-s); 
  line-height: var(--system-text-body-line-height);
`;
