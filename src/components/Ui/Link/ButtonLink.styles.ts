import styled from "styled-components";
import { BodyText } from "../Text";

export const ButtonStyled = styled.button`
    padding: 1.5rem 2.5rem;
    width: fit-content;
    cursor: pointer;
`;

export const ImgStyled = styled.img`
    display: block;
    height: 1.5rem;
    width: 1.8rem;
    margin-left: 2rem;
    filter: var(--system-color-logo-nav);
`;

export const ButtonContentStyled = styled.span`
    ${BodyText}

    font-weight: var(--system-weight-bold);
    display: flex;
`;
