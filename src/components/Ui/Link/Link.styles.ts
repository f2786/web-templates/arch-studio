import styled from "styled-components";
import { BodyText } from "../Text";

export const StyledLink = styled.a`
    ${BodyText}

    cursor: pointer;
    font-weight: var(--system-weight-bold);
    color: var(--system-color-link);

    :hover {
        color: var(--system-color-link-hover);
    }
`;
