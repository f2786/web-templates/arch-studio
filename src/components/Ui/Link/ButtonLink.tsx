import PropTypes from "prop-types";
import React from "react";
import { ButtonContentStyled, ButtonStyled, ImgStyled } from "./ButtonLink.styles";

// eslint-disable-next-line max-len
const ButtonLink: React.FC = React.forwardRef<HTMLButtonElement, React.ComponentProps<typeof ButtonStyled>>(({ children, ...props }, ref) => (
  <ButtonStyled ref={ref} {...props}>
    <ButtonContentStyled>
      <span>{children}</span>
      <ImgStyled src="/icons/icon-arrow.svg" />
    </ButtonContentStyled>
  </ButtonStyled>
));

ButtonLink.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ButtonLink;
