import { forwardRef, ReactNode, ComponentPropsWithRef } from "react";
import NextLink from "next/link";
import PropTypes from "prop-types";
import { StyledLink } from "./Link.styles";

export type LinkProps = {
  href: string;
  children: ReactNode;
  openNewTab?: boolean;
  nextLinkProps?: Omit<LinkProps, "href">;
} & ComponentPropsWithRef<"a">;

const Link = forwardRef<HTMLAnchorElement, LinkProps>(({
  // eslint-disable-next-line react/prop-types
  children, href, openNewTab, nextLinkProps, ...rest
}, ref) => {
  const isNewTab = openNewTab !== undefined
    ? openNewTab
    : href && !href.startsWith("/") && !href.startsWith("#");

  if (!isNewTab) {
    return (
      <NextLink href={href} {...nextLinkProps}>
        <StyledLink ref={ref} {...rest}>
          {children}
        </StyledLink>
      </NextLink>
    );
  }

  return (
    <StyledLink
      ref={ref}
      target="_blank"
      rel="noopener noreferrer"
      href={href}
      {...rest}
    >
      {children}
    </StyledLink>
  );
});

Link.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string.isRequired,
  openNewTab: PropTypes.bool,
};

Link.defaultProps = {
  openNewTab: false,
};

export default Link;
