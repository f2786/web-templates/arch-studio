import { withA11y } from "@storybook/addon-a11y";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import React from "react";
import GlobalStyles from "../../../styles/global.styles";
import Switch from "./Switch";

export default {
  title: "Components/Switch",
  component: Switch,
  decorators: [withA11y],
  argTypes: {
    checked: {
      control: { type: "boolean" },
    },
  },
} as ComponentMeta<typeof Switch>;

const Styled: React.FC = ({ children }) => (
  <>
    <GlobalStyles />
    {children}
  </>
);

const Template: ComponentStory<typeof Switch> = (args) => (
  <Styled>
    <Switch {...args} />
  </Styled>
);

export const Basic = Template.bind({});
Basic.args = {
  checked: true,
  label: "Settings",
};
