/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import tw from "twin.macro";
import PropTypes from "prop-types";
import styled from "styled-components";
import { motion as Motion } from "framer-motion";
import {
  Thumb, Root, SwitchProps, SwitchThumbProps,
} from "@radix-ui/react-switch";

interface ComponentProps extends Omit<SwitchProps, "defaultChecked"> {
  label?: string;
}

const RootWrapper = styled(Root)<ComponentProps>(() => [
  tw`relative inline-flex flex-shrink-0 h-[1.95rem] w-[4rem]`,
  tw`border-[0.1rem] border-transparent rounded-full cursor-pointer`,
  tw`focus-visible:ring-white focus-visible:ring-opacity-75`,
  tw`focus:outline-none focus-visible:ring-2`,
  // checked ? tw`bg-teal-700` : tw`bg-teal-900`,
]);

const ThumbWrapper = styled(Thumb)<SwitchThumbProps>(() => [
  tw`pointer-events-none inline-block h-[1.75rem] w-[1.75rem]`,
  tw`rounded-full bg-white shadow-lg`,
]);

const variants = {
  right: { transform: "translateX(2rem)" },
  left: { transform: "translateX(0rem)" },
};

const Switch: React.FC<ComponentProps> = ({
  checked,
  label,
  ...rest
}) => (
  <RootWrapper
    checked={checked}
    data-testid="Switch"
    {...rest}
  >
    <ThumbWrapper
      // This causes 2 re-renders
      as={Motion.span}
      variants={variants}
      animate={checked ? "right" : "left"}
    />
    <span tw="sr-only">{label}</span>
  </RootWrapper>
);

Switch.defaultProps = {
  label: "Settings",
};

Switch.propTypes = {
  label: PropTypes.string,
};

export default Switch;
