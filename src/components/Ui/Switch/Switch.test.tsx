/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Switch from "./Switch";

test("renders all options passed to it", () => {
  const handleChange = jest.fn();
  const { getByTestId } = render(<Switch checked={false} onCheckedChange={handleChange} />);

  expect(getByTestId("Switch")).toBeTruthy();

  fireEvent.click(getByTestId("Switch"));
  fireEvent.click(getByTestId("Switch"));

  expect(handleChange).toHaveBeenCalledTimes(2);
});

test("renders homepage unchanged", () => {
  const { container } = render(<Switch checked={false} />);

  expect(container).toMatchSnapshot();
});
