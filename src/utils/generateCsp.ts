import { v4 } from "uuid";
import { createHash } from "crypto";

// https://stackoverflow.com/questions/65551212/using-csp-in-nextjs-nginx-and-material-uissr
export const generateCsp = (): [string, string] => {
  const production = process.env.NODE_ENV === "production";

  // generate random nonce converted to base64. Must be different on every HTTP page load
  const nonce = createHash("sha256").update(v4()).digest("base64");

  let csp = "";
  csp += "default-src 'none';";
  csp += "base-uri 'self';";
  csp += "form-action 'self';";
  csp += "img-src 'self';";
  csp += "style-src https://fonts.googleapis.com 'unsafe-inline';"; // NextJS requires 'unsafe-inline'
  csp += `script-src 'nonce-${nonce}' 'self' ${production ? "" : "'unsafe-inline' 'unsafe-eval'"};`; // NextJS requires 'self' and 'unsafe-eval' in dev (faster source maps)
  csp += "font-src https://fonts.gstatic.com;";

  if (!production) {
    csp += "connect-src 'self' ws://localhost:3000;";
  }

  return [csp, nonce];
};
