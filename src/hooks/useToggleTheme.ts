import { useTheme } from "next-themes";

type ReturnType = [string | undefined, () => void] | []

const useToggleTheme = (): ReturnType => {
  // Adds 2 extra re-renders when it is used
  const { setTheme, resolvedTheme } = useTheme();

  if (!resolvedTheme) return [];

  const toggleTheme = () => {
    setTheme(resolvedTheme === "light" ? "dark" : "light");
  };

  return [resolvedTheme, toggleTheme];
};

export default useToggleTheme;
