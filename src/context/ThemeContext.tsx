import { useTheme } from "next-themes";
import { useEffect, useState } from "react";

type ReturnType = [string | undefined, () => void] | []

export const useThemeContext = (): ReturnType => {
  const [mounted, setMounted] = useState(false);
  const { setTheme, resolvedTheme } = useTheme();

  useEffect(() => setMounted(true), []);

  if (!mounted) return [];

  const toggleTheme = () => {
    setTheme(resolvedTheme === "light" ? "dark" : "light");
  };

  return [resolvedTheme, toggleTheme];
};
