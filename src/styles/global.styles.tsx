import React from "react";
import { createGlobalStyle } from "styled-components";
import resetStyles from "./reset.styles";

const CustomStyles = createGlobalStyle`
  ${resetStyles}

  html {
    background: var(--system-color-page);
    box-sizing: border-box;
    font-size: var(--system-text-base-size);
    font-feature-settings: "liga", "clig";
    font-variant-ligatures: common-ligatures;
    line-height: 1;
    letter-spacing: -0.04em;
    -moz-osx-font-smoothing: grayscale;
    overflow-x: hidden;
    overflow-y: auto;
    text-rendering: optimizeLegibility;
    text-size-adjust: 100%;
    word-break: break-word;
    -webkit-font-smoothing: antialiased;
  }

  

  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    font-size: 100%;
    vertical-align: baseline;
    box-sizing: inherit;
    text-size-adjust: inherit;
  }

  body,
  #__next,
  html {
    width: 100%;
    min-height: 100vh;
    -webkit-tap-highlight-color: transparent;
  }

  :root {
    /* Font */
    --system-font-family: 'Spartan', sans-serif;
    --system-text-base-size: 18px;
    --system-text-size-increment: 1.2;
    --system-text-title-line-height: 1;
    --system-text-body-line-height: 1.5rem;
    --system-text-xxs: calc(var(--system-text-xs) / var(--system-text-size-increment));
    --system-text-xs: calc(var(--system-text-s) / var(--system-text-size-increment));
    --system-text-s: calc(var(--system-text-base-size) / var(--system-text-size-increment)); /* 15px */
    --system-text-b: var(--system-text-base-size); /* 18px */
    --system-text-m: calc(var(--system-text-b) * var(--system-text-size-increment)); /* 21.6px */
    --system-text-l: calc(var(--system-text-m) * var(--system-text-size-increment)); /* 25.92px */
    --system-text-xl: calc(var(--system-text-l) * var(--system-text-size-increment)); /* 31px */
    --system-text-2xl: calc(var(--system-text-xl) * var(--system-text-size-increment)); /* 37px */
    --system-text-3xl: calc(var(--system-text-2xl) * var(--system-text-size-increment)); /* 44px */
    --system-text-4xl: calc(var(--system-text-3xl) * var(--system-text-size-increment)); /* 53px */
    --system-text-5xl: calc(var(--system-text-4xl) * var(--system-text-size-increment)); /* 64px */
    --system-text-6xl: calc(var(--system-text-5xl) * var(--system-text-size-increment)); /* 77px */
    --system-text-7xl: calc(var(--system-text-6xl) * var(--system-text-size-increment)); /* 92px */
    --system-text-8xl: calc(var(--system-text-7xl) * var(--system-text-size-increment)); /* 111px */
    --system-text-9xl: calc(var(--system-text-8xl) * var(--system-text-size-increment)); /* 133px */
    --system-text-10xl: calc(var(--system-text-9xl) * var(--system-text-size-increment)); /* 160px */
    --system-text-11xl: calc(var(--system-text-10xl) * var(--system-text-size-increment)); /* 192px */

    /* Depth */
    --system-depth-high: 10;
    --system-depth-higher: 20;
    --system-depth-highest: 30;
    --system-depth-beforeNavigation: 9999;
    --system-depth-navigation: 10000;
    --system-depth-afterNavigation: 10001;
    --system-depth-beforeOverlay: 19999;
    --system-depth-overlay: 20000;
    --system-depth-afterOverlay: 20001;


    --system-palette-oceanLightest: hsl(208, 100%, 90%);
    --system-palette-oceanLighter: hsl(212, 100%, 59%);
    --system-palette-oceanLight: hsl(214, 100%, 54%);
    --system-palette-ocean: hsl(216, 100%, 50%);
    --system-palette-oceanDark: hsl(218, 100%, 47%);
    --system-palette-oceanDarker: hsl(220, 100%, 43%);
    --system-palette-oceanDarkest: hsl(224, 100%, 18%);
    --system-palette-blueLightest: hsl(196, 100%, 90%);
    --system-palette-blueLighter: hsl(200, 100%, 59%);
    --system-palette-blueLight: hsl(202, 100%, 54%);
    --system-palette-blue: hsl(204, 100%, 50%);
    --system-palette-blueDark: hsl(206, 100%, 47%);
    --system-palette-blueDarker: hsl(208, 100%, 43%);
    --system-palette-blueDarkest: hsl(212, 100%, 20%);
    --system-palette-skyLightest: hsl(188, 100%, 89%);
    --system-palette-skyLighter: hsl(192, 100%, 59%);
    --system-palette-skyLight: hsl(194, 100%, 54%);
    --system-palette-sky: hsl(196, 100%, 50%);
    --system-palette-skyDark: hsl(198, 100%, 47%);
    --system-palette-skyDarker: hsl(200, 100%, 43%);
    --system-palette-skyDarkest: hsl(204, 100%, 20%);
    --system-palette-tealLightest: hsl(172, 73%, 88%);
    --system-palette-tealLighter: hsl(176, 88%, 64%);
    --system-palette-tealLight: hsl(178, 80%, 56%);
    --system-palette-teal: hsl(180, 73%, 50%);
    --system-palette-tealDark: hsl(182, 73%, 47%);
    --system-palette-tealDarker: hsl(184, 73%, 43%);
    --system-palette-tealDarkest: hsl(188, 73%, 18%);
    --system-palette-greenLightest: hsl(166, 80%, 88%);
    --system-palette-greenLighter: hsl(162, 76%, 58%);
    --system-palette-greenLight: hsl(161, 92%, 45%);
    --system-palette-green: hsl(160, 100%, 40%);
    --system-palette-greenDark: hsl(159, 100%, 37%);
    --system-palette-greenDarker: hsl(158, 100%, 33%);
    --system-palette-greenDarkest: hsl(152, 100%, 14%);
    --system-palette-yellowLightest: hsl(52, 100%, 88%);
    --system-palette-yellowLighter: hsl(48, 100%, 61%);
    --system-palette-yellowLight: hsl(46, 100%, 56%);
    --system-palette-yellow: hsl(44, 100%, 50%);
    --system-palette-yellowDark: hsl(42, 100%, 46%);
    --system-palette-yellowDarker: hsl(40, 100%, 42%);
    --system-palette-yellowDarkest: hsl(36, 100%, 20%);
    --system-palette-orangeLightest: hsl(41, 100%, 90%);
    --system-palette-orangeLighter: hsl(37, 100%, 61%);
    --system-palette-orangeLight: hsl(35, 100%, 56%);
    --system-palette-orange: hsl(33, 100%, 50%);
    --system-palette-orangeDark: hsl(31, 100%, 46%);
    --system-palette-orangeDarker: hsl(30, 100%, 42%);
    --system-palette-orangeDarkest: hsl(26, 100%, 20%);
    --system-palette-redLightest: hsl(337, 100%, 94%);
    --system-palette-redLighter: hsl(341, 100%, 71%);
    --system-palette-redLight: hsl(343, 100%, 66%);
    --system-palette-red: hsl(345, 100%, 60%);
    --system-palette-redDark: hsl(347, 82%, 54%);
    --system-palette-redDarker: hsl(349, 72%, 46%);
    --system-palette-redDarkest: hsl(353, 100%, 18%);
    --system-palette-pinkLightest: hsl(328, 100%, 94%);
    --system-palette-pinkLighter: hsl(332, 100%, 75%);
    --system-palette-pinkLight: hsl(334, 100%, 71%);
    --system-palette-pink: hsl(336, 100%, 67%);
    --system-palette-pinkDark: hsl(338, 80%, 60%);
    --system-palette-pinkDarker: hsl(338, 59%, 51%);
    --system-palette-pinkDarkest: hsl(346, 100%, 18%);
    --system-palette-purpleLightest: hsl(265, 100%, 94%);
    --system-palette-purpleLighter: hsl(260, 100%, 74%);
    --system-palette-purpleLight: hsl(259, 100%, 71%);
    --system-palette-purple: hsl(258, 100%, 67%);
    --system-palette-purpleDark: hsl(257, 80%, 60%);
    --system-palette-purpleDarker: hsl(256, 62%, 52%);
    --system-palette-purpleDarkest: hsl(252, 69%, 20%);
    
    /* Shadows */
    --system-shadow-1: 0px 1px 2px 0px rgba(0, 0, 0, 0.1), 0px 2px 10px 0px rgba(0, 0, 0, 0.08);
    --system-shadow-2: 0 6px 12px 0 rgba(0, 0, 0, 0.04), 0 0 0 1px rgba(0, 0, 0, 0.04);
    --system-shadow-3: 0 6px 8px 0 rgba(0, 0, 0, 0.08), 0 0 0 1px rgba(0, 0, 0, 0.04);
    --system-shadow-4: 0 8px 12px 0 rgba(0, 0, 0, 0.08), 0 0 0 1px rgba(0, 0, 0, 0.04);
    --system-shadow-5: 0px 48px 132px -32px rgba(54, 14, 73, 0.08);
    
    /* Spacing */
    --system-space-0: 0px;
    --system-space-1: 4px;
    --system-space-2: 8px;
    --system-space-3: 12px;
    --system-space-4: 16px;
    --system-space-5: 24px;
    --system-space-6: 32px;
    --system-space-7: 48px;
    --system-space-8: 64px;
    --system-space-9: 88px;

    --system-spacing-1: 0.5rem;
    --system-spacing-2: 1rem;
    --system-spacing-3: 1.5rem;
    --system-spacing-4: 2rem;
    --system-spacing-5: 2.5rem;
    --system-spacing-6: 3rem;
    --system-spacing-7: 3.5rem;
    --system-spacing-8: 4rem;
    --system-spacing-9: 4.5rem;
    --system-spacing-10: 5rem;

    /* Font weights */
    --system-weight-regular: 400;
    --system-weight-medium: 500;
    --system-weight-bold: 700;
    --system-weight-black: 800;
    
    /* widths */
    --system-width-mobile-content: 311px;
    --system-width-mobile-menu: 343px;

    /* Max widths */
    --system-width-mobile: 375px;
    --system-width-tablet-small: 574px;
    --system-width-tablet: 768px;
    --system-width-desktop-small: 960px;
    --system-width-desktop: 1110px;
    --system-width-desktop-large: 1280px;

    /* Colors */
    /* --system-color-primary: #1B1D23; */
    --system-color-gray-dark: #60636D;
    --system-color-gray-medium: #7D828F;
    --system-color-gray-light: #C8CCD8;
    --system-color-gray-2light: #EEEFF4;
    --system-color-warning: #DF5656;

    /* Svg colors  */
    --system-color-svg-light: brightness(1000%);
    --system-color-svg-dark: invert(0%);


    /* Light theme */

    /* Main Page */
  --system-color-page: #ffffff;
  /* TODO */
  --system-color-logo-nav: var(--system-color-svg-dark);
  --system-color-logo-footer: var(--system-color-svg-light);

  /* Text */
  --system-color-title: #1B1D23;

  /* Link */
  --system-color-link: #7D828F;
  --system-color-link-hover: #1B1D23;

  /* Footer */
  --system-color-footer: #EEEFF4;
  
  /* Button */
  --system-color-button-image: #1B1D23;
  --system-color-button-image-content: #fff;
  --system-color-button-image-logo: var(--system-color-svg-light);

  --system-color-button-carousel: #fff;
  --system-color-button-carousel-active: #1B1D23;
  --system-color-button-carousel-content: #7D828F;
  --system-color-button-carousel-content-active: #fff;
  --system-color-button-carousel-hover: #EEEFF4;
  --system-color-button-carousel-hover-active: #7D828F;
  
  --system-color-button-page: #1B1D23;
  --system-color-button-page-content: #fff;
  --system-color-button-page-logo: var(--system-color-svg-light);
  }

  body {
    scroll-behavior: smooth;
    font-family: var(--system-font-family);
  }

[data-theme="light"] {
  /* Main Page */
  --system-color-page: #ffffff;
  /* TODO */
  --system-color-logo-nav: var(--system-color-svg-dark);
  --system-color-logo-footer: var(--system-color-svg-light);

  /* Text */
  --system-color-title: #1B1D23;

  /* Link */
  --system-color-link: #7D828F;
  --system-color-link-hover: #1B1D23;

  /* Footer */
  --system-color-footer: #EEEFF4;
  
  /* Button */
  --system-color-button-image: #1B1D23;
  --system-color-button-image-content: #fff;
  --system-color-button-image-logo: var(--system-color-svg-light);

  --system-color-button-carousel: #fff;
  --system-color-button-carousel-active: #1B1D23;
  --system-color-button-carousel-content: #7D828F;
  --system-color-button-carousel-content-active: #fff;
  --system-color-button-carousel-hover: #EEEFF4;
  --system-color-button-carousel-hover-active: #7D828F;
  
  --system-color-button-page: #1B1D23;
  --system-color-button-page-content: #fff;
  --system-color-button-page-logo: var(--system-color-svg-light);
}

[data-theme="dark"] {
  /* Main Page */
  --system-color-page: #1B1D23;
  /* TODO */
  --system-color-logo-nav: var(--system-color-svg-light);
  --system-color-logo-footer: var(--system-color-svg-dark);
  
  /* Text */
  --system-color-title: #fff;

  /* Link */
  --system-color-link: #C8CCD8;
  --system-color-link-hover: #fff;

  /* Footer */
  --system-color-footer: #292b33;

  /* Button */
  --system-color-button-image: #1B1D23;
  --system-color-button-image-content: #fff;
  --system-color-button-image-logo: var(--system-color-svg-light);
  --system-color-button-page: #fff;
  --system-color-button-page-content: #1B1D23;
  --system-color-button-page-logo: var(--system-color-svg-dark);

  --system-color-button-carousel: #1B1D23;
  --system-color-button-carousel-active: #fff;
  --system-color-button-carousel-content: #fff;
  --system-color-button-carousel-content-active: #1B1D23;
  --system-color-button-carousel-hover: #7D828F; 
  --system-color-button-carousel-hover-active: #d9dae0;

 

  /* To change */
  /* --system-color-primary: #1B1D23; */
  --system-color-gray-dark: #60636D;
  --system-color-gray-medium: #7D828F;
  --system-color-gray-light: #C8CCD8;
  --system-color-gray-2light: #EEEFF4;
  --system-color-warning: #DF5656;
  }

`;

const GlobalStyles = () => <CustomStyles />;

export default GlobalStyles;
