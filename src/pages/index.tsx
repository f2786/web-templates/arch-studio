import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import getT from "next-translate/getT";
import PropTypes from "prop-types";
import { Carousel } from "../components/Carousel";
import { FeaturedProjects } from "../components/FeaturedProjects";
import { WelcomeSection } from "../components/Marketing";
import { Banner } from "../components/Marketing/Banner";
import { PageTitle } from "../components/PageTitle";

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const pt = await getT(locale, "portfolio");
  const portfolioContent = pt("content", null, { returnObjects: true });
  const ct = await getT(locale, "carousel");
  const carouselContent = ct("content", null, { returnObjects: true });

  return {
    props: {
      portfolioContent,
      carouselContent,
    },
    revalidate: 86400,
  };
};
interface CarouselData {
    img: string;
    title: string;
    description: string;
    link: {
      url: string;
      content: string;
    },
}

type CarouselContent = CarouselData[]
interface PortfolioData {
  title: string;
  description: string;
  src: string;
  srcSet: string;
  alt: string;
}

export type PortfolioContent = PortfolioData[]

interface PageProps {
  portfolioContent: PortfolioContent
  carouselContent: CarouselContent
}

const Home: NextPage<PageProps> = ({ portfolioContent, carouselContent }) => (
  <>
    <Head>
      <title>Arch Studio</title>
      <meta
        name="description"
        content="Your team of professionals"
      />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <PageTitle title="home" />
    <Carousel content={carouselContent} />
    <WelcomeSection />
    <Banner />
    <FeaturedProjects content={portfolioContent} />
  </>
);

Home.propTypes = {
  portfolioContent: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    srcSet: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
  }).isRequired).isRequired,
  carouselContent: PropTypes.arrayOf(PropTypes.shape({
    img: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    link: PropTypes.shape({
      url: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired).isRequired,
};

export default Home;
