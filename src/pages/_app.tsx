import type { AppProps } from "next/app";
import Head from "next/head";
import { ThemeProvider } from "next-themes";
import GlobalStyles from "../styles/global.styles";
import { Layout } from "../components/Common";

const MyApp = ({ Component, pageProps }: AppProps) => (
  <>
    <Head>
      <meta
        name="viewport"
        content="initial-scale=1.0, width=device-width, maximum-scale=1,user-scalable=no"
      />
    </Head>
    <GlobalStyles />
    <ThemeProvider defaultTheme="system">
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>

  </>
);

export default MyApp;
