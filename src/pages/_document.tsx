import Document, {
  DocumentContext, Html, Head, Main, NextScript,
} from "next/document";
import { ServerStyleSheet } from "styled-components";
import { generateCsp } from "../utils";

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () => originalRenderPage({
        enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
      });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    const [csp, nonce] = generateCsp();

    return (
      <Html lang="en">

        <Head nonce={nonce}>
          <meta property="csp-nonce" content={nonce} />
          <meta httpEquiv="Content-Security-Policy" content={csp} />
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
          <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700;800&display=swap" rel="stylesheet" />

          {/* PWA primary color */}
          {/* <meta name='theme-color' content={theme.palette.primary.main} /> */}
          <meta charSet="iso-8859-1" />
          <meta name="HandheldFriendly" content="True" />
          <meta name="MobileOptimized" content="320" />
          <meta name="format-detection" content="telephone=no" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="black"
          />
          <meta httpEquiv="pragma" content="cache" />
        </Head>
        <body>
          <Main />
          <NextScript nonce={nonce} />
        </body>
      </Html>
    );
  }
}
