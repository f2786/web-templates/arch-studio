// eslint-disable-next-line import/no-extraneous-dependencies
const withBundleAnalyzer = require("@next/bundle-analyzer")({
    enabled: process.env.BUNDLE_ANALYSE === "true",
});
const nextTranslate = require("next-translate");
const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require("next/constants");
// eslint-disable-next-line import/no-extraneous-dependencies
const withPlugins = require("next-compose-plugins");
// next.config.js
const securityHeaders = [
    /**
     * This header controls DNS prefetching, allowing browsers
     * to proactively perform domain name resolution on external
     * links, images, CSS, JavaScript, and more. This prefetching
     * is performed in the background, so the DNS is more likely to
     * be resolved by the time the referenced items are needed.
     * This reduces latency when the user clicks a link.
     */
    {
        key: "X-DNS-Prefetch-Control",
        value: "on",
    },
    /**
     * This header informs browsers it should only be accessed using HTTPS,
     * instead of using HTTP. Using the configuration below, all present and
     * future subdomains will use HTTPS for a max-age of 2 years. This blocks
     * access to pages or subdomains that can only be served over HTTP.
     */
    {
        key: "Strict-Transport-Security",
        value: "max-age=63072000; includeSubDomains; preload",
    },
    /**
     * This header stops pages from loading when they detect reflected cross-site
     * scripting (XSS) attacks. Although this protection is not necessary when sites
     * implement a strong Content-Security-Policy disabling the use of inline JavaScript
     * ('unsafe-inline'), it can still provide protection for older web browsers that
     * don't support CSP.
     */
    {
        key: "X-XSS-Protection",
        value: "1; mode=block",
    },
    /**
     * This header indicates whether the site should be allowed to be displayed within an iframe.
     * This can prevent against clickjacking attacks. This header has been superseded by CSP's
     * frame-ancestors option, which has better support in modern browsers.
     */
    {
        key: "X-Frame-Options",
        value: "SAMEORIGIN",
    },
    /**
     * This header allows you to control which features and APIs can be used in the browser.
     * It was previously named Feature-Policy. You can view the full list of permission
     * options here.
     */
    {
        key: "Permissions-Policy",
        value: "camera=(), microphone=(), geolocation=(), interest-cohort=()",
    },
    /**
     * This header prevents the browser from attempting to guess the type of content if
     * the Content-Type header is not explicitly set. This can prevent XSS exploits for
     * websites that allow users to upload and share files. For example, a user trying to
     * download an image, but having it treated as a different Content-Type like an executable,
     * which could be malicious. This header also applies to downloading browser extensions.
     * The only valid value for this header is nosniff.
     */
    {
        key: "X-Content-Type-Options",
        value: "nosniff",
    },
    /**
     * This header controls how much information the browser includes when navigating from the
     * current website (origin) to another.
     */
    // https://scotthelme.co.uk/a-new-security-header-referrer-policy/
    {
        key: "Referrer-Policy",
        value: "origin-when-cross-origin",
    },
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
];

/** @type {import('next').NextConfig} */
const envConfig = (phase) => {
    const isDev = phase === PHASE_DEVELOPMENT_SERVER;
    const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== "1";
    const isStaging = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === "1";

    const env = {
        CSP_BASE_URI: (() => {
            if (isDev) return "http://localhost:3000";
            if (isProd) return "https://www.mydomain.com";
            if (isStaging) return "https://staging.mydomain.com";

            return "CSP_BASE_URI:not (isDev,isProd && !isStaging,isProd && isStaging)";
        })(),
    };

    return {
        env,
    };
};

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    styledComponents: true,
    experimental: {
        reactRemoveProperties: true,
        removeConsole: true,
    },
    async headers() {
        return [
            {
                source: "/:path*",
                headers: securityHeaders,
            },
        ];
    },
    generateBuildId: async () => process.env.COMMIT_SHA ?? "my-build-id",
};

module.exports = withPlugins([[withBundleAnalyzer], [nextTranslate], [envConfig]], nextConfig);
